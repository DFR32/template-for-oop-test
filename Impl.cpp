#include <iostream>
#include <fstream>
#include <cstdint>
#include <tuple>

#include "Impl.hpp"

int main()
{
	using std::cout;
	using std::cin;

	int32_t action;

	for (;;)
	{
		std::string w;
		std::getline(cin, w);

		// Example: Maluma, 2.32, 2
		auto tup = IO::Parse<std::string, double, unsigned, std::string>(w, "[,;:-]+");

		cout << std::get<0>(tup) << ' ' << std::get<1>(tup) << ' ' << std::get<2>(tup) << ' ' << std::get<3>(tup) << '\n';
		break;

		// print message
		cout << "Bine ai venit\n";
		cout << "Pentru limba romana, apasati tasta 1.\n";
		cout << "Pentru limba engleza, apasati tasta 2.\n";

		// read input
		cin >> action;
		
		// decide action
		switch (action)
		{
			case 1:
				cout << "Ati ales limba romana.\n";
				break;

			case 2:
				cout << "You chose english.\n";
				break;

			default:
				cout << "Tasta nu corespunde unei comenzi valide.\n";
				break;
		}
	}
	return 0;
}