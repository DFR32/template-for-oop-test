#pragma once
#include <fstream>
#include <cstdint>
#include <iostream>
#include <string>
#include <type_traits>
#include <vector>
#include <regex>
#include <tuple>
#include <cctype>

namespace IO
{
	template <typename T>
	T IO_cast(const std::string& substring)
	{
		return static_cast<T>(substring);
	}

	template <>
	double IO_cast<double>(const std::string& substring)
	{
		return std::stof(substring);
	}

	template <>
	unsigned IO_cast<unsigned>(const std::string& substring)
	{
		return std::stoul(substring);
	}

	template <>
	int IO_cast<int>(const std::string& substring)
	{
		return std::stoi(substring);
	}

	template <>
	std::string IO_cast<std::string>(const std::string& substring)
	{
		std::string copy_str(substring);

		// Remove trailing or leading spaces
		if (std::isspace(copy_str[0]))
			copy_str.erase(copy_str.begin());
		if (std::isspace(copy_str[copy_str.size() - 1]))
			copy_str.erase(copy_str.begin() + copy_str.size() - 1);

		return copy_str;
	}

	std::vector<std::string> split(const std::string& str, const std::string& regexp)
	{
		std::regex re(regexp);
		std::vector<std::string> container{ std::sregex_token_iterator(str.begin(), str.end(), re, -1),
			std::sregex_token_iterator() };
		return container;
	}

	template <typename T>
	std::tuple<T> reduce(std::vector<std::string>& str_vec)
	{
		if (str_vec.empty())
			return {};

		T t;
		t = IO_cast<T>(str_vec[0]);
		str_vec.erase(str_vec.begin());
		return std::tuple<T>(std::move(t));
	}

	template <typename T, typename Arg, typename... Args>
	std::tuple<T, Arg, Args...> reduce(std::vector<std::string>& str_vec)
	{
		if (str_vec.empty())
			return {};

		T t;
		t = IO_cast<T>(str_vec[0]);
		str_vec.erase(str_vec.begin());
		return std::tuple_cat(std::tuple<T>(std::move(t)), reduce<Arg, Args...>(str_vec));
	}

	template <typename... Args>
	std::tuple<Args...> parse_as_tuple(std::vector<std::string> str_vec)
	{
		return IO::reduce<Args...>(str_vec);
	}

	template <typename... Outputs>
	std::tuple<Outputs...> Parse(const std::string& str, const std::string& regexp)
	{
		const size_t parameter_pack_sz = sizeof...(Outputs);
		auto split_vector = IO::split(str, regexp);

		if (parameter_pack_sz != split_vector.size())
			return {};

		return parse_as_tuple<Outputs...>(split_vector);
	}
}

class DummyClass
{
private:
	uint32_t dummy;

public:
	DummyClass()
		:
		dummy()
	{}

	~DummyClass() = default;

	friend std::ostream& operator<<(std::ostream& out, const DummyClass& other);
	friend std::istream& operator>>(std::istream& in, const DummyClass& other);
};

template <typename T>
class Dummy
{
private:
	uint32_t dummy;

public:
	Dummy()
		:
		dummy()
	{}

	~Dummy()
	{}

	friend std::ostream& operator<<(std::ostream& out, const Dummy<T>& other);
	friend std::istream& operator>>(std::istream& in, const Dummy<T>& other);
};

template<>
class Dummy<uint32_t>
{
private:
	uint32_t dummy;

public:
	Dummy()
		:
		dummy()
	{}

	~Dummy()
	{}

	friend std::ostream& operator<<(std::ostream& out, const Dummy<uint32_t>& other);
	friend std::istream& operator>>(std::istream& in, const Dummy<uint32_t>& other);
};